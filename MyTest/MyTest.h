#ifndef MYTEST_H
#define MYTEST_H


#include <sstream>
#include <iostream>

#include <assert.h>

namespace MyStrCon {
    const std::string RED    = "\033[1;31m";
    const std::string GREEN  = "\033[1;32m";
    const std::string BLUE   = "\033[1;34m";
    const std::string YELLOW = "\033[1;33m";
    const std::string PURPLE = "\033[1;35m";
    const std::string GREY   = "\033[1;30m";
    const std::string STD    = "\033[0m";
}

/**
 * @brief The MyTest Klasa zawiera przydatne funkcje przy pisaniu testow.
 */
class MyTest
{
    unsigned int testNb;

    static unsigned int allTests; //!< zmienna przechowuje info o sumie wykoanych testow
                                  //!< we wszystkich instancjach MyTest

public:
    MyTest();
    ~MyTest();

    /**
     * @brief printTestTitle funkcja wypisuje tytul testu
     * @param m_iTestTitle
     * @return
     */
    static std::string printTestTitle(const std::string& m_iTestTitle) noexcept;

    /**
     * @brief printTitle funkcja wypisuje tytul pewnego fragmentu testu z jego numerem
     * @param m_iTestTitle
     * @return
     */
    std::string printTitle(const std::string& m_iTestTitle) noexcept;

    /**
     * @brief printOk funkcja wypisuje komunikat ok
     * @return
     */
    std::string printOk() noexcept;

    /**
     * @brief printTestTitle funkcja wypisuje tytul testu
     * @param m_iTestTitle
     * @return
     */
    void coutTestTitle(const std::string& m_iTestTitle) noexcept;

    /**
     * @brief printAllTests funkcja wyswietla podsumowanie testow
     * @return
     */
    static std::string printSummary() noexcept;

    /**
     * @brief printTitle funkcja wypisuje tytul pewnego fragmentu testu z jego numerem
     * @param m_iTestTitle
     * @return
     */
    void coutTitle(const std::string& m_iTestTitle, bool endl=true) noexcept;

    /**
     * @brief printOk funkcja wypisuje komunikat ok
     * @return
     */
    void coutOk() noexcept;

    /**
     * @brief coutSummary funkcja wypisuje podsumowanie
     */
    static void coutSummary();


    /**
     *  funkcja zwraca prawde jezeli wynik wypisania obiektu obj strumieniem jest taki jak
     *  expectedString
     */
    template<class T>
    static bool chcekStream(const T& obj, const std::string& expectedString, bool print=false) noexcept;

    static bool stringCompare(const std::string &s1, const std::string &s2);
};

template<class T>
bool MyTest::chcekStream(const T &obj, const std::string &expectedString, bool print) noexcept
{
    std::stringstream str;

    str<<obj;

    if(print) std::cout<<obj;

    bool ret = expectedString == str.str();

    if(ret == false){
        std::cout<<obj;
    }

    return ret;
}

#endif // MYTEST_H
