#include "MyTest.h"

unsigned int MyTest::allTests = 0;

MyTest::MyTest()
{
    testNb = 0;
}

MyTest::~MyTest()
{
    allTests+=testNb;
}

std::string MyTest::printTestTitle(const std::string &m_iTestTitle) noexcept
{
    unsigned int lineQtty = (80-m_iTestTitle.size())/2;

    std::string m_ret="\n";

    for(size_t i=0;i<lineQtty;++i)
        m_ret+="-";

    m_ret += MyStrCon::BLUE+m_iTestTitle+MyStrCon::STD;

    for(size_t i=0;i<lineQtty;++i)
        m_ret+="-";

    return m_ret;
}

std::string MyTest::printTitle(const std::string &m_iTestTitle) noexcept
{
    testNb++;
    return "  "+MyStrCon::PURPLE+std::to_string(testNb)+")"+MyStrCon::BLUE+" "+m_iTestTitle+MyStrCon::STD;
}

std::string MyTest::printOk() noexcept
{
    return MyStrCon::GREEN+"ok"+MyStrCon::STD;
}

std::string MyTest::printSummary() noexcept
{
    return printTestTitle("Podsumowanie")+"\n * Lacznie wykonano "
            +MyStrCon::GREEN+std::to_string(allTests)+MyStrCon::STD+" testow";
}

void MyTest::coutTestTitle(const std::string &m_iTestTitle) noexcept
{
    std::cout<<printTestTitle(m_iTestTitle)<<std::endl;
}

void MyTest::coutTitle(const std::string &m_iTestTitle, bool endl) noexcept
{
    std::cout<<printTitle(m_iTestTitle);

    if(endl) std::cout<<std::endl;
}

void MyTest::coutOk() noexcept
{
    std::cout<<printOk()<<std::endl;
}

void MyTest::coutSummary()
{
    std::cout<<printSummary()<<std::endl;
}

bool MyTest::stringCompare(const std::string& s1, const std::string& s2)
{
    if(s1!=s2){
        std::cout<<"S1: "<<s1<<std::endl;
        std::cout<<"S2: "<<s2<<std::endl;
        return false;
    }
    return true;
}
